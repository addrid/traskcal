+++
title = "01 Úvodem"
description = "Úvodem o projektu Trask DevOps Academy - projekt hypoteční kalkulačka"
alwaysopen = true
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 1
+++

# Ještě než začneme
Něco málo o tom, co je potřeba připravit než vůbec začneme programovat. 

## Hlavní myšlenky DevOps akademie
- Zjistit, v čem se chci dále rozvíjet, jaké role mě zajímají
- Získat základní přehled, jak věci fungují na projektech
- Zjistit, na jaké kolegy z Trasku se lze obracet při hledání pomoci
- A hlavně to nejdůležitější - **dělat to, co nás opravdu baví :)**

## Co připravit
- Rozdělit si v týmu projektové role
- Domluvit si (pokud možno) pravidelné schůzky
- Zajistit si potřebné nástroje
- Vytvořit a udržovat dokumentový server
- Využít GitLab k organizaci zdrojových kódů
- Nastavit si lokální i vzdálené vývojové prostředí

## Cíle projektu
- Implementovat praktický příklad - **Úvěrovou kalkulačku**
- Vyzkoušet si v týmu řídit, analyzovat, navrhovat, programovat, testovat a získávat zpětnou vazbu od kolegů

## Projektové role
- **Projektový manažer**
   - Co chceme?
   - Proč to chceme?
   - Jak to chceme?
   - S kým to chceme?
   - Kdy to chceme?
   - Za kolik to chceme?
- **Analytik**
   - Jak to vypadá?
   - Jak to běží?
   - Jaká jsou tam data?
   - ... a kde je těch 90 % neviditelné temné hmoty / energie?
- **Architekt / vývojář**
   - Z čeho se skládá?
   - Data (Model)
   - Chování (Controller)
   - Vizualizace (View)
- **Tester**
   - Funguje to?
   - Je to výkonné?
   - Je to bezpečné?
----