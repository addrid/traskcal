+++
title = "02 DevOps"
description = "O DevOps obecně, teoreticky"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 2
+++

# DevOps
Níže je možné najít teoretický základ pro pochopení, co pojem DevOps představuje.

Jedná se spíše o teoretický základ hodnot DevOps, kterých bychom se měli držet.

## DevOps infinity loop - fáze
- Code - vývoj a review, nástroje pro řízení zdrojového kódu, mergování kódu
- Build - nástroje nepřetržité integrace, stavy buildů
- Test - nástroje nepřetržitého testování poskytující zpětnou vazbu na business rizika
- Package - repozitář artefaktů, příprava aplikace před nasazením
- Release - change management, schvalování releasů, automatizace releasů
- Configure - konfigurace infrastruktury a řízení, nástroje infrastruktury jako kódu
- Monitor - monitorování výkonnosti aplikace, 

![DevOps Infinite Loop](https://gitlab.com/Illner/LoanCalculatorTrask/raw/master/public/images/devops.png "DevOps Infinite Loop")

## Hlavní myšlenky DevOps - C\*A\*M\*S
- **C**ulture - lidé, procesy, nástroje
- **A**utomation - infrastruktura jako kód
- **M**easurement . měřit vše
- **S**haring - kolaborace a zpětná vazba

## Pojmy DevOps
- **Continuous integration** (CI)
   - Typicky automaticky spouštěný proces složený z propojených kroků jako kompilace kódu, spouštění unit a akceptančních testů, validace kódu, ověřování dodržení standardů a buildování deployment balíčků
- **Continuous delivery** (CD)
   - Praktika nepřetržitého nasazování kvalitních SW buildů na určitá prostředí
- **Continuous deployment** (CD)
   - Nepřetržité nasazování implikuje nepřetržité dodávání a je praktikou zajišťující nepřetržitou připravenost SW k vypuštění mezi aktuální zákazníky.

![DevOps CD pipeline](https://gitlab.com/Illner/LoanCalculatorTrask/raw/master/public/images/cd_pipeline.png "Continuous Delivery Pipeline")
 
## Chápání plýtvání u DevOps (D.O.W.N.T.I.M.E.)
- **D**efects - např. Špatně navržený a nekvalitní kód, nefunkční výkonnostní problémy, ...
- **O**verproduction - např. Dodávání funkcionalit, o které nemají zákazníci zájem, ...
- **W**aiting - např. Přehnaně složité backlogy, nepřipravená infrastruktura a data pro testování, ...
- **N**on-value added processing - např. Dlouhé řešení problémů, "hašení požárů", ... 
- **T**ransportation - např. Časté rollbacky releasů, předávky mezi QA a vývojem
- **I**nventory - např. Nevyužité zdroje, částečně dokončená práce či přebytek nedokončené práce
- **M**otion - např. Neustálé výměny vývojářů, způsobující prodlevy při učení nového či předělávání stávající práce
- **E**mployee knowledge (unused) - např. Nedostatek zpětné vazby od manažerů, uzavřená retrospektiva a standup meetingy.