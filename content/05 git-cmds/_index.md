+++
title = "05 Práce s Gitem"
description = "Užitečné příkazy při používání Gitu"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 5
+++

# Možnosti práce
1. Využití [Git příkazů](https://git-scm.com/downloads) v příkazové řádce
2. Využití Git GUI nástrojů (např. [TortoiseGit](https://tortoisegit.org/) pro windows)

## Příkazy Git
![Git architecture](../imgs/git_flow.png "Architektura gitu")
Pozn.: Na obrázku je ilustrovaná práce pouze v rámci jedné gitové branche, je však vhodné větvení projektu rozumně využít.[*[zdroj obrázku](https://www.edureka.co/blog/git-tutorial/)*]

### Vytváření nového či získání existujícího projektu
1. Vytvoření nového (lokálního) repozitáře
```
git init
```
2. Naklonování existujícího (vzdáleného) repozitáře (přes SSH či HTTPS)
```
git clone git@gitlab.com:[username]/[repozitář].git
```
```
git clone https://gitlab.com/[username]/[repozitář].git
```

### Snapshotting
1. Zjištění stavu
```
git status
```

2. Přidání všech změn v projektu do přípravné oblasti
```
git add -A
```

3. Commit včetně (věcné) informační hlášky
```
git commit -m "[zpráva u commitu]"
```

### Větvení projektu
1. Zobrazení všech existujíích větví
```
git branch -a
```

2. Vytvoření nové větve
```
git branch [název_větve]
```

3. Smazání větve
```
git branch -d [název_větve]
```

4. Vytvoření nové větve a změna aktuální větve na vytvořenou
```
git checkout -b [název_větve]
```

5. Změna aktuální větve
```
git checkout [název_větve]
```

### Mergování
1. Merge zvolené větve do aktuální
```
git merge [název_větve]
```

2. Merge zvolené větve do aktuální
```
git merge [zdrojová_větev] [cílová_větev]
```


### Aktualizace a sdílení projektu
1. Aktualizace lokálního repozitáře na nejnovější commit
```
git pull
```

2. Aktualizace lokálního repozitáře ze specifické větve
```
git pull origin [název_větve]
```

3. Pushnutí větve do totožné větve vzdáleného repozitáře
```
git push
```

4. Pushnutí aktuální větve do specifické větve ve vzdáleném repozitáři
```
git push origin [název_větve]
```

### Inspekce a porovnání
1. Zobrazení změn
```
git log
```

2. Zobrazení detailních změn
```
git log --summary
```

3. Zobrazení rozdílů (změn) před mergováním
```
git diff [zdrojová_větev] [cílová_větev]
```

