+++
type="page"
title = "07 01 Checklist pro PM"
description = "Návrh checklistu pro projektového manažera"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
+++

Možná podoba checklistu projektového manažera pro inspiraci:

- Ví všichni, co je předmětem jejich práce?
- Je vytvořený komunikační kanál pro náš tým?
- Je zadaná práce?
- Stíháme dodat výsledek?

