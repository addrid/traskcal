+++
type="page"
title = "07 Checklisty"
description = "Návrh checklistů pro jednotlivé role"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 7
+++




# Checklisty
Využití checklistů je zcela dobrovolné, ovšem může se jevit jako užitečná pomůcka pro zajištění, že jsme na nic nezapomněli.
