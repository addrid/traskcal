+++
type="page"
title = "07 04 Checklist pro TEST"
description = "Návrh checklistu pro testera"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
+++


Možná podoba checklistu testera pro inspiraci:

- Mám k dispozici seznam požadavků?
- Mám k dispozici testovanou aplikaci?
- Mám připravené nástroje pro zápis výsledků testů a možných defektů?
- Je vytvořen komunikační kanál mezi testerem, analytikem a vývojářem?


