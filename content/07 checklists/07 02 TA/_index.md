+++
type="page"
title = "07 02 Checklist pro TA"
description = "Návrh checklistu pro (test) analytika"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
+++


Možná podoba checklistu (test) analytika pro inspiraci:

- Vím, jak má vypadat výstup naší práce?
- Znám kontext projektu a vím, s kým konzultovat návrh řešení?
- Vedu si přehledný seznam požadavků?
- Je seznam požadavků přístupný i ostatním členům týmu?
- Informoval jsem tým o svém dosavadním postupu?
- Definoval jsem všechny požadavky dostatečně natolik, aby pokryly všechny potřeby?


