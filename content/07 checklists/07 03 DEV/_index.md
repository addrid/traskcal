+++
type="page"
title = "07 03 Checklist pro DEV"
description = "Návrh checklistu pro vývojáře/architekta"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
+++


Možná podoba checklistu vývojáře/architekta pro inspiraci:

- Mám k dispozici seznam požadavků, podle kterých budu kalkulačku implementovat?
- Mám připravené všechny potřebné nástroje?
- Vedu si zdrojové kódy v přehledné struktuře na GitLabu?
- Komunikuji s testerem a řeším případné problémy?
- Jsou implementovaný GitLab Pages?


