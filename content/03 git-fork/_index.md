+++
title = "03 Forknutí projektu"
description = "Jak si forknout GitLabový projekt"
creatordisplayname = "Petr Novák"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 3
+++


* předpokládám, že už sis založil účet na Gitlabu
* přihlas se a přejdi na [hlavní stránku](https://gitlab.com/Illner/LoanCalculatorTrask) repozitáře
* v pravém horním rohu je tlačítko **fork**. Klikni na něj.
* vyskočí výběr jmenného prostoru pro nový projekt. Klikni položky s tvým uživatelským jménem
* Nový repozitář je vytvořen a měl by ses ocitnout na jeho hlavní stránce