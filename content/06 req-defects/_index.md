+++
type="page"
title = "06 Požadavky a defekty"
description = "Doporučení jak pracovat s požadavky"
creatordisplayname = "Daniel Navrátil"
creatoremail = "navratd@gmail.com"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 6
+++




# Požadavky
- Před tím, než začneme programovat, je potřebné mít samozřejmě sepsané požadavky.
- Za ty je zodpovědný analytik vašeho týmu a vývojáři (a taktéž testerovi) by měl tyto požadavky dodat v rozumné formě a v rozumném čase.
- V praxi je možné vidět různé zápisy požadavků:
  - U tradičně řízených projektů (vodopády, ...) vidíme nejčastěji zápis ve formě [Případů užití](http://mpavus.wz.cz/uml/uml-b-use-case-3-2-1.php) (Use Case), modelovaných např. v UML
    - Popis případu
    - Identifikátor případu
    - Účastníci
    - Vstupní podmínky
    - Tok události
    - Výstupní podmínky
    - Alternativní tok
    - Chybový tok
  - U agilně řízených projektů (scrum, ...) pak využíváme zápis ve formě [User Stories](https://searchsoftwarequality.techtarget.com/definition/user-story)
    - Jako *\<role>* chci *\<co/funkce>*, protože *\<důvod>*
    - Jako *uživatel* chci *mít možnost volby výše úvěru*, abych si *vypůjčil přesně tolik, kolik potřebuji*.

- Pro zápis požadavků je možné využít modul **Issues** v GitLabu (není to však nutnou podmínkou, lze využít i jiné nástroje - např. [Trello](https://trello.com/))
- Preferovanou metodou zápisu je forma User Stories
  - BONUS: jedním z možných rozšíření user stories je zápis akceptačních kritérií v tzv. [Gherkin](https://cucumber.io/docs/gherkin/reference/) syntaxi
  - Ta definuje využití struktury GIVEN - WHEN - THEN pro lepší definici různého chování, kterým funkcionalita / požadavek disponuje
