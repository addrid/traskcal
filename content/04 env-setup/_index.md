+++
title = "04 Nastavení prostředí"
description = "Jak si nastavit lokální prostředí"
creatordisplayname = "Petr Novák"
lastmodifierdisplayname = "Daniel Navrátil"
lastmodifieremail = "navratd@gmail.com"
weight = 4
+++


# Vývojové prostředí
Je potřeba si na počítač nainstalovat nějaké vývojové prostředí nebo textový editor, ve kterém budeš kalkulačku programovat. 
Na školení možná dostanete instalační soubor Webstormu pro Windows. 
Webstorm se dá nainstalovat i na Linux a Mac. Trial verze je na 30 dní zdarma. 
Se studentskou licencí se dá stáhnout zadarmo.

Jako alternativu doporučuji textový editor [Visual Studio Code](https://code.visualstudio.com/). 
Dá se stáhnout zadarmo na všechny platformy. 
Nemá tak dobré schopnosti napovídání jako Webstorm, ale je rychlejší, dají se do něj doinstalovat šikovná rozšíření a jednoduše se v něm pracuje s Gitem.

## Git a NodeJS
Je potřeba si na počítači nainstalovat Git a NodeJS.

### Návod pro Windows a Mac
1. stáhni si instalační soubor Gitu z [této](https://git-scm.com/downloads) stránky
2. soubor spusť a projdi instalačním wizardem. Bližší info najdeš v [tomto](https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/) návodu
3. na [této](https://nodejs.org/en/download/) stránce si vyber vhodný instalační soubor pro tvůj počítač a stáhni jej
4. soubor spusť a projdi instalačním wizardem. Bližší info najdeš v [tomto](https://www.guru99.com/download-install-node-js.html) návodu

### Návod pro Linux (Ubuntu a Debian)
1. přejdi do příkazové řádky
2. pokud není Git už předinstalovaný, nainstaluj ho:
```
sudo apt install git
```
3. nainstaluj NodeJS a NPM
```
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install npm@latest -g
```
4. je potřeba předem globálně nainstalovat i Angular:
```
npm install -g @angular/cli
```

## Samotný repozitář
Už si stačí jen stáhnout projekt, nainstalovat závislosti a spustit.
1. spusť příkazovou řádku a přejdi do složky, ve které chceš mít projekt (lze přes Webstorm terminál)
```
cd <absolutní cesta ke složce>
```
2. vytvoř libovolný adresář
```
mkdir <název adresáře>
```
3. přejdi do vytvořeného adresáře
```
cd <název adresáře>
```
4. do vytvořené složky naklonuj tvůj gitlab repozitář. Cestu ke git souboru najdeš na hlavní stránce repozitáře. V pravém horním rohu klikni na "clone" jedna i druhá cesta by měly fungovat.
```
git clone <cesta ke git souboru>
```
5. přejdi do adresáře s naklonovaným projektem. Pokud nevíš, jaký je název adresáře, použij příkaz **dir** na Windows nebo **ls** na Macu a Linuxu pro zobrazení obsahu složky ve které se teď nacházíš
```
cd <název stáhlé složky>
```
6. stáhni a nainstaluj všechny závislosti
```
npm install
```
7. po dokončení již můžeš spustit server
```
npm start
```
8. klient potom běží na **```http:localhost:4200```**
